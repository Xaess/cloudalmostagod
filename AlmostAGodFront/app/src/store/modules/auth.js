import Vue from 'vue'
import Vuex from 'vuex'
import userService from '../../services/userService'

Vue.use(Vuex)

export default {
  state: {
    user: null
  },
  actions: {
    authenticate ({ commit, dispatch }, user) {
      return userService.authenticate(user).then(response => {
        localStorage.setItem('user', JSON.stringify(user))
        commit('setUser', response.data)
        commit('setUniverse', response.data.universe)
      }).catch(err => {
        throw err
      })
    },
    register ({ commit, dispatch }, user) {
      return userService.register(user).then(response => {
        localStorage.setItem('user', JSON.stringify(user))
      }).catch(err => {
        throw err
      })
    },
    logout ({ commit }) {
      commit('setUser', null)
      localStorage.removeItem('user')
    }
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    updateTeam (state, team) {
      state.user.team = team
    }
  },
  getters: {
    isAuthenticated (state) {
      return state.user !== null
    },
    getCurrentUser (state) {
      return state.user
    }
  }
}
