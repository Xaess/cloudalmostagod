import leaderboardService from '../../services/leaderboardService'

export default {
  state: {
    leaderboard: []
  },
  actions: {
    fetchLeaderboard ({ rootState, state, commit }) {
      return leaderboardService.index().then(response => {
        commit('updateLeaderboard', response.data)
      })
    }
  },
  getters: {
    getLeaderboard (state) {
      return state.leaderboard
    }
  },
  mutations: {
    updateLeaderboard (state, leaderboard) {
      state.leaderboard = leaderboard
    }
  }
}
