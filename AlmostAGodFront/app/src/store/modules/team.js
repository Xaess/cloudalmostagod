import teamService from '../../services/teamService'

export default {
  state: {
    teams: []
  },
  actions: {
    createTeam ({ rootState, state, commit }, name) {
      if (rootState.user !== null) {
        return teamService.create({ teamName: name }).then(response => {
          commit('updateTeam', response.data.team)
        })
      }
    },
    updateTeam ({ rootState }) {
      return teamService.update({ id: rootState.auth.user.team._id, total_microwave: rootState.auth.user.universe.microwave })
    },
    fetchTeams ({ rootState, state, commit }) {
      return teamService.index().then(response => {
        commit('updateTeams', response.data)
      })
    },
    joinTeam ({ rootState, state, commit }, name) {
      const index = state.teams.findIndex(x => x.name === name)
      return teamService.join(state.teams[index]._id).then(response => {
        commit('updateTeam', state.teams[index])
      })
    }
  },
  getters: {
    getTeams (state) {
      return state.teams
    }
  },
  mutations: {
    updateTeams (state, teams) {
      state.teams = teams
    }
  }
}
