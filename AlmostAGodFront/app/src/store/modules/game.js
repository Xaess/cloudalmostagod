import universeService from '../../services/universeService'

export default {
  state: {
    universe: null,
    game: {
      microwaves: 0,
      atoms: 10000000,
      max_atoms: 0,
      stars: {
        number: 0,
        basePrice: 2,
        baseMicrowave: 2,
        atomGain: 1,
        name: 'Star'
      },
      constellations: {
        number: 0,
        basePrice: 200,
        baseMicrowave: 200,
        atomGain: 2000,
        name: 'Constellation'
      },
      galaxies: {
        number: 0,
        basePrice: 20000,
        baseMicrowave: 20000,
        atomGain: 100,
        name: 'Galaxy'
      },
      nebulas: {
        number: 0,
        baseMicrowave: 200000,
        basePrice: 200000,
        atomGain: 200,
        name: 'Nebula'
      }
    }
  },
  actions: {
    fetchUniverse ({ commit }, id) {
      universeService.show(id).then(response => {
        commit('setUniverse', response.universe)
      })
    },
    saveUniverse ({ rootState, commit }) {
      if (rootState.auth.user !== null) {
        if (rootState.auth.user.universe !== null) {
          const id = rootState.auth.user.universe._id
          return universeService.save({ id: id, universe: rootState.auth.user.universe }).then(response => {})
        }
      }
    },
    gainAtom ({ state, commit }) {
      if (state.universe !== null) {
        let atomsEarned = 0
        for (let i = 0; i < state.universe.constructs.length; i++) {
          if (state.universe.constructs[i].number > 0) {
            atomsEarned += state.universe.constructs[i].number * state.universe.constructs[i].atomGain
          }
        }
        if ((atomsEarned + state.universe.atom) > state.game.max_atoms) {
          commit('updateMaxAtoms', (atomsEarned + state.universe.atom))
        }
        commit('updateAtoms', (atomsEarned + state.universe.atom))
        return 'gained ' + atomsEarned + ' atoms'
      }
    },
    buy ({ state, commit }, construct) {
      const index = state.universe.constructs.findIndex(x => x._id === construct._id)
      if (state.universe.atom >= (state.universe.constructs[index].number > 0 ? state.universe.constructs[index].number : 1) * state.universe.constructs[index].basePrice) {
        commit('updateConstructs', { number: state.universe.constructs[index].number + 1, index: index })
        commit('updateAtoms', state.universe.atom - (state.universe.constructs[index].number * state.universe.constructs[index].basePrice))
        commit('updateMicrowaves', state.universe.microwave + state.universe.constructs[index].baseMicrowave)
      }
    },
    prestige ({ state, commit }) {
      return universeService.prestige().then(response => {
        commit('setUniverse', response.data)
      })
    }
  },
  getters: {
    getAtoms (state) {
      if (state.universe !== null) {
        return state.universe.atom
      }
    },
    getMicrowaves (state) {
      if (state.universe !== null) {
        return state.universe.microwave
      }
    },
    getMaxAtoms (state) {
      return state.game.max_atoms
    },
    getAtomsPerSecond (state) {
      if (state.universe !== null) {
        let atomPerSecond = 0
        for (let i = 0; i < state.universe.constructs.length; i++) {
          atomPerSecond += (state.universe.constructs[i].number * state.universe.constructs[i].atomGain)
        }
        return Math.floor(atomPerSecond)
      }
    },
    getConstructs (state) {
      if (state.universe !== null) {
        return state.universe.constructs
      }
    }
  },
  mutations: {
    setUniverse (state, universe) {
      state.universe = universe
    },
    updateMicrowaves (state, microwaves) {
      state.universe.microwave = Math.round(microwaves * 10) / 10
    },
    updateAtoms (state, atoms) {
      state.universe.atom = Math.round(atoms * 10) / 10
    },
    updateMaxAtoms (state, atoms) {
      state.game.max_atoms = Math.round(atoms * 10) / 10
    },
    updateConstructs (state, params) {
      state.universe.constructs[params.index].number = params.number
    }
  }
}
