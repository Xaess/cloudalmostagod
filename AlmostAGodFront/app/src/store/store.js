import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import game from './modules/game'
import team from './modules/team'
import leaderboard from './modules/leaderboard'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    team,
    leaderboard,
    game
  }
})
