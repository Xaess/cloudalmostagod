import axios from 'axios'

const create = function (body) {
  return axios.post(process.env.VUE_APP_API_ENDPOINT + '/teams', body).then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    console.log(err)
    throw err
  })
}

const update = function (body) {
  return axios.patch(process.env.VUE_APP_API_ENDPOINT + '/teams/' + body.id, body).then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    console.log(err)
    throw err
  })
}

const join = function (teamID) {
  return axios.patch(process.env.VUE_APP_API_ENDPOINT + '/teams/join/' + teamID).then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    throw err
  })
}

const index = function () {
  return axios.get(process.env.VUE_APP_API_ENDPOINT + '/teams').then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    throw err
  })
}

export default {
  index,
  create,
  join,
  update
}
