import axios from 'axios'

const show = function (id) {
  return axios.get(process.env.VUE_APP_API_ENDPOINT + '/universes/' + id).then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    console.log(err)
    throw err
  })
}

const save = function (body) {
  return axios.patch(process.env.VUE_APP_API_ENDPOINT + '/universes/' + body.id, body.universe).then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    console.log(err)
    throw err
  })
}

const prestige = function () {
  return axios.post(process.env.VUE_APP_API_ENDPOINT + '/universes/prestige').then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    console.log(err)
    throw err
  })
}

export default {
  show,
  prestige,
  save
}
