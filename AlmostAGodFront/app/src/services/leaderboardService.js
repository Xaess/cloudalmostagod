import axios from 'axios'

const index = function () {
  return axios.get(process.env.VUE_APP_API_ENDPOINT + '/leaderboard/').then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    console.log(err)
    throw err
  })
}

export default {
  index
}
