import axios from 'axios'

const authenticate = function (user) {
  return axios.post(process.env.VUE_APP_API_ENDPOINT + '/users/login', user).then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    console.log(err)
    throw err
  })
}

const register = function (user) {
  return axios.post(process.env.VUE_APP_API_ENDPOINT + '/users/register', user).then(response => {
    if (response.data !== null) {
      return response.data
    }
  }).catch(err => {
    throw err
  })
}

export default {
  authenticate,
  register
}
