import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Auth from './views/Auth.vue'
import Team from './views/Team.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'auth',
      component: Auth
    },
    {
      path: '/team',
      name: 'team',
      component: Team
    },
    {
      path: '*',
      name: 'home',
      component: Home
    }

  ]
})
