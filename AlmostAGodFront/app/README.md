# Almost a God Synopsis

Ce document à pour but d'exposer le synopsis du projet élaborer dans le cadre du projet du cours de NodeJS de la majeur Software Intelligence de l'EFREI.

## Synthèse du sujet

Le projet à pour but de collaborer par groupe de 4 afin de construire une application en mode Client-Serveur, qui soit à la fois ludique, pointue et performante. Afin d'avoir une application qui nous mette face à une certaine complexité algorithmique nous avons choisi le thème des clickers. Un clicker est un type de jeu très particulier, il est aussi parfois appelé jeu incremental et se base sur des formules et des algorythmes complexes afin d'offrir aux joueurs une progression exponentielle dans le jeu. Sur navigateur il existe quelques clickers connus, dont "cookie clicker" ou encore "clicker heroes". Afin d'apporter une touche d'innovation à ce sujet nous avons choisi de se baser sur une progression en coopération en temps réel, notre but est donc que les joueurs puisse former des équipes et s'entraider pour progresser dans le jeux sur une sauvegarde commune. 
La conception du jeu offrira la mise en place de concept  simple, comme la creation de profils de joueurs, la conception d'équipe ou la sauvegarde de la progression des équipes. Mais elle offrira également des concept plus pointu comme la mise en place des systèmes de cooperation entre des joueurs en temps réel ou la conception et l'équilibrage des formules mathématique qui géreront la progressions des joueurs.

## Synthèse de Conception

Le sujet choisi étant un jeu, les interactions avec l'utilisateur se situe au coeur du projet, nous avons donc choisi de construire le client du projet avec VueJS. Il s'agit d'un framework NodeJS permettant de construire des application web dites "one page",  à la fois rapide, puissante et légère. Ce type  de framework offre une experience utilisateur optimale.
Pour pouvoir stocker les données des utilisateurs nous avons choisi de mettre en place une API REST NodeJS/ExpressJS et d'utiliser comme base de donnée MongoDB.

Ce type d'architecture est également appelée "MEVN Stack" (Mongo ExpressJS, VueJS, NodeJS) et convient parfaitement aux besoins du projet.

Pour la mise en production et l'hébergement nous comptons utiliser les différentes technologies d'AWS.


## RoadMap

Afin de veiller a la bonne réalisation du projet nous avons décider de nous aider des outils de gestion de projets disponible sur Gitlab afin de créer des issues et les regrouper par milestones.
Nous avons donc élaborer une liste de milestones :

- deuxième semaine d'octobre : Mise en place des projets (création de l'API REST NodeJS/Express, de la base de donnée MongoDB ainsi que de l'application web VueJS)
- troisième semaine d'octobre : Mise en place des fonctionnalité de base sur l'API et le Front (creation de compte, d'équipe etc)
- dernière semaine d'octobre : Création des assets graphique (images logo etc)
- dernière semaine d'octobre :  Sauvegarde de la progression
- première semaine de novembre : Mise en place du système de coopération en temps réel
- première semaine de novembre : Elaboration des formules incrémentales
- deuxième semaine de novembre : Equilibrage et corrections

## Objectifs du prototype

La version du projet disponible mi novembre sera un prototype, nos objectif pour ce prototypes sont : 

- La possibilité de créer un compte et une équipe 
- La possibilité de gérer son compte ou son équipe 
- La possibilité de rejoindre une équipe déjà crée par un autre utilisateur
- La possibilité de jouée a plusieurs sur une même partie en temps réel et  sans conflits
- L'élaboration d'une interface claire facilitant la comprehension du jeu
- La possibilité pouvoir progresser dans le jeu.
