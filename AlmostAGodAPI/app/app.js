let bodyParser = require('body-parser')
let mongoose = require('mongoose')
let express = require('express')
const cors = require('cors')
const session = require('express-session')
const templateFactory = require('./factory/templateFactory')
let hostname = 'localhost'
let port = 3000

let app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
var config = require('config')

app.use(
  cors({
    origin: [ config.cors ],
    methods: [ 'GET', 'POST', 'PATCH', 'DELETE' ],
    credentials: true // enable set cookie
  })
)
/**
 * Connect to database
 */

mongoose.connect(config.mongodb, { useNewUrlParser: true }).then(
  () => {
    console.log('Connected to database agg on port : 27017')
  },
  (err) => {
    console.error(err)
  }
)

// initialize express-session to allow us track the logged-in user across sessions.
app.use(
  session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false
    }
  })
)

/**
 * import routes
 */
let authRoutes = require('./routes/api-auth-routes')
let constructRoutes = require('./routes/api-construct-routes')
let universeRoutes = require('./routes/api-universe-routes')
let alienRoutes = require('./routes/api-alien-routes')
let templateRoutes = require('./routes/api-template-routes')
let teamRoutes = require('./routes/api-team-routes')
let leaderboardRoutes = require('./routes/api-leaderboards-router')
app.use('/api/users', authRoutes)
app.use('/api/universes', universeRoutes)
app.use('/api/constructs', constructRoutes)
app.use('/api/aliens', alienRoutes)
app.use('/api/teams', teamRoutes)
app.use('/api/template', templateRoutes)
app.use('/api/leaderboard', leaderboardRoutes)
const server = app.listen(port, hostname, function () {
  templateFactory.populate()
  console.log('Server listening on port :' + port)
})

app.use(express.static('uploads'))

module.exports = server
