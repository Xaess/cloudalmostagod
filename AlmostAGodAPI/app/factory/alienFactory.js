const Alien = require('../models/alienModel')
const AlienConstruct = require('../models/alienTemplateModel')

exports.create = async function () {
  const aliens = await AlienConstruct.find()
  let table = []
  for (const alien of aliens) {
    let newAlien = new Alien()
    newAlien.number = alien.number
    newAlien.name = alien.name
    newAlien.baseStardust = alien.baseStardust
    newAlien.atomGain = alien.atomGain

    await newAlien.save()
    table.push(newAlien)
  }
  return table
}
