const ConstrcutsTemplateModel = require('../models/constructTemplateModel')
exports.populate = async function () {
  let constructsTemplates = require('../data/templateConstruct.json')
  for (let template in constructsTemplates) {
    let templateExist = await ConstrcutsTemplateModel.findOne({ name: constructsTemplates[template].name })
    if (!templateExist) {
      let constructTemplate = new ConstrcutsTemplateModel()
      constructTemplate.number = constructsTemplates[template].number
      constructTemplate.basePrice = constructsTemplates[template].basePrice
      constructTemplate.baseMicrowave = constructsTemplates[template].baseMicrowave
      constructTemplate.atomGain = constructsTemplates[template].atomGain
      constructTemplate.name = constructsTemplates[template].name
      await constructTemplate.save()
    }
  }
}
