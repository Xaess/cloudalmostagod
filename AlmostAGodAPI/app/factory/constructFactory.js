const Construct = require('../models/constructModel')
const TemplateConstruct = require('../models/constructTemplateModel')

exports.create = async function () {
  const constructs = await TemplateConstruct.find()
  let table = []
  for (const construct of constructs) {
    let newconstruct = new Construct()
    newconstruct.number = construct.number
    newconstruct.name = construct.name
    newconstruct.basePrice = construct.basePrice
    newconstruct.baseMicrowave = construct.baseMicrowave
    newconstruct.atomGain = construct.atomGain
    await newconstruct.save()
    table.push(newconstruct)
  }
  return table
}
