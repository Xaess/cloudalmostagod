var mongoose = require('mongoose')

var constructSchema = mongoose.Schema({
  number: {
    type: Number,
    default: 1
  },
  name: {
    type: String,
    required: true
  },
  basePrice: {
    type: Number,
    default: 1
  },
  baseMicrowave: {
    type: Number,
    default: 1
  },
  atomGain: {
    type: Number,
    default: 1
  }
})

var Construct = (module.exports = mongoose.model('Construct', constructSchema))

module.exports.get = function (callback, limit) {
  Construct.find(callback).limit(limit)
}
