var mongoose = require('mongoose')

var alienSchema = mongoose.Schema({
  number: {
    type: Number,
    default: 1
  },
  name: {
    type: String,
    required: true
  },
  base_stardust: {
    type: Number,
    default: 1
  },
  atomGain: {
    type: Number,
    default: 1
  }
})

var Alien = (module.exports = mongoose.model('Alien', alienSchema))

module.exports.get = function (callback, limit) {
  Alien.find(callback).limit(limit)
}
