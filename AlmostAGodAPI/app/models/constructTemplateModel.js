var mongoose = require('mongoose')

var constructSchema = mongoose.Schema({
  number: {
    type: Number,
    default: 1
  },
  name: {
    type: String,
    required: true
  },
  basePrice: {
    type: Number,
    default: 1
  },
  baseMicrowave: {
    type: Number,
    default: 1
  },
  atomGain: {
    type: Number,
    default: 1
  }
})

var TemplateConstruct = (module.exports = mongoose.model('TemplateConstruct', constructSchema))

module.exports.get = function (callback, limit) {
  TemplateConstruct.find(callback).limit(limit)
}
