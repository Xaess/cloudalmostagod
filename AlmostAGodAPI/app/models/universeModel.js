var mongoose = require('mongoose')

var universeSchema = mongoose.Schema({
  constructs: [ { type: mongoose.Schema.Types.ObjectId, ref: 'Construct' } ],
  aliens: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Alien' }],
  atom: {
    type: Number,
    default: 2
  },
  microwave: {
    type: Number,
    default: 0
  },
  atomSec: {
    type: Number,
    default: 0
  },
  stardust: {
    type: Number,
    default: 0
  }
})

var Universe = (module.exports = mongoose.model('Universe', universeSchema))

module.exports.get = function (callback, limit) {
  Universe.find(callback).limit(limit)
}
