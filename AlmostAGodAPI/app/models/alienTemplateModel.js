const mongoose = require('mongoose')

const alienSchema = mongoose.Schema({
  level: {
    type: Number,
    default: 1
  },
  name: {
    type: String,
    required: true
  },
  base_stardust: {
    type: Number,
    default: 1
  },
  atomSec: {
    type: Number,
    default: 1
  }
})

const TemplateAlien = (module.exports = mongoose.model('TemplateAlien', alienSchema))

module.exports.get = function (callback, limit) {
  TemplateAlien.find(callback).limit(limit)
}
