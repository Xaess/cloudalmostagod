var mongoose = require('mongoose')

var teamSchema = mongoose.Schema({
  name: String,
  users: [ { type: mongoose.Schema.Types.ObjectId, ref: 'User' } ],
  total_microwave: {
    type: Number,
    default: 0
  }
})

var Team = (module.exports = mongoose.model('Team', teamSchema))

module.exports.get = function (callback, limit) {
  Team.find(callback).limit(limit)
}
