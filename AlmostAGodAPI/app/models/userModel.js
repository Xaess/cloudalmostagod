var mongoose = require('mongoose')

var userSchema = mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  team: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' },
  universe: { type: mongoose.Schema.Types.ObjectId, ref: 'Universe' }
})

var User = (module.exports = mongoose.model('User', userSchema))

module.exports.get = function (callback, limit) {
  User.find(callback).limit(limit)
}
