const Team = require('../models/teamModel')
const User = require('../models/userModel')

exports.index = async function (req, res) {
  if (!req.session.user) {
    return res
      .status(500)
      .send({
        status: 'error',
        data: 'FORBIDDEN'
      })
      .end()
  }

  try {
    let teams = await Team.find()
    if (!teams || teams.lenght === 0) {
      return res.status(404).send({
        status: 'error',
        data: 'No teams found'
      })
    }
    return res.json({
      status: 'success',
      data: teams
    })
  } catch (err) {
    console.log(err.message)
    return res.status(500).send({
      status: 'error',
      data: err
    })
  }
}

exports.get = async function (req, res) {
  if (!req.session.user) {
    return res
      .status(500)
      .send({
        status: 'error',
        data: 'FORBIDDEN'
      })
      .end()
  }

  try {
    let team = await Team.findById(req.params.team_id)
    if (!team) {
      return res.status(404).send({
        status: 'error',
        data: 'Team not found'
      })
    }

    return res.json({
      status: 'success',
      data: team
    })
  } catch (err) {
    return res.status(500).send({
      status: 'error',
      data: err
    })
  }
}

exports.update = async function (req, res) {
  if (!req.session.user) {
    return res
      .status(500)
      .send({
        status: 'error',
        data: 'FORBIDDEN'
      })
      .end()
  }

  try {
    let team = await Team.findById(req.params.team_id)
    if (!team) {
      return res
        .status(404)
        .send({
          status: 'error',
          data: 'Team not found'
        })
        .end()
    }

    team.total_microwave = team.total_microwave + parseFloat(req.body.total_microwave) || team.total_microwave
    await team.save()

    return res.json({
      status: 'success',
      data: team
    }).end
  } catch (err) {
    console.log(err.message)
    res.status(500).send({
      status: 'error',
      data: err
    })
  }
}

exports.create = async function (req, res) {
  if (!req.body.teamName) {
    return res.status(404).send({
      status: 'error',
      data: 'Make sure that you send all the required data'
    })
  }
  if (!req.session.user) {
    return res
      .status(500)
      .send({
        status: 'error',
        data: 'FORBIDDEN'
      })
      .end()
  }

  try {
    let user = await User.findById(req.session.user._id)
    if (user.team) {
      return res.status(423).send({
        status: 'error',
        data: 'User is already in a team'
      })
    }

    let existingTeamWithGivenName = await Team.findOne({ name: req.body.teamName })

    if (existingTeamWithGivenName) {
      return res.status(423).send({
        status: 'error',
        data: 'This team name is already used.'
      })
    }

    let team = new Team()
    team.name = req.body.teamName
    team.users.push(req.session.user)
    team = await team.save()

    user.team = team
    user = await user.save()
    user.password = null

    return res.json({
      status: 'success',
      data: {
        team: team,
        user: user
      }
    })
  } catch (err) {
    console.log(err.message)
    return res.status(500).send({
      status: 'error',
      data: err
    })
  }
}

exports.join = async function (req, res) {
  if (!req.session.user) {
    return res
      .status(500)
      .send({
        status: 'error',
        data: 'FORBIDDEN'
      })
      .end()
  }

  if (req.session.user.team) {
    res.status(423).send({
      status: 'error',
      data: 'User is already in a Team'
    })
  }

  try {
    let team = await Team.findById(req.params.team_id)
    if (!team) {
      return res.status(500).send({
        status: 'error',
        data: 'Team not found'
      })
    }
    let user = await User.findById(req.session.user._id)
    team.users.push(user)
    team = await team.save()
    user.team = team
    user = await user.save()
    user.password = null

    return res.json({
      status: 'success',
      data: {
        team: team,
        user: user
      }
    })
  } catch (err) {
    console.log(err.message)
  }
}

exports.leave = async function (req, res) {
  if (!req.session.user) {
    return res
      .status(500)
      .send({
        status: 'error',
        data: 'FORBIDDEN'
      })
      .end()
  }

  try {
    let user = User.findById(req.session.user._id)

    if (!user.team) {
      return res.status(404).send({
        status: 'error',
        data: "User doesn't have team."
      })
    }

    let userTeam = Team.findById(user.Team)

    user.team = null
    user = await user.save()

    let tmpTeamUsers = []

    userTeam.forEach((element) => {
      if (String(element._id) !== req.session.user._id) {
        tmpTeamUsers.push(element)
      }
    })

    userTeam.users = tmpTeamUsers
    userTeam = await userTeam.save()

    return res.json({
      status: 'success',
      data: {
        team: userTeam,
        user: user
      }
    })
  } catch (err) {
    console.log(err.message)
    return res.status(500).send({
      status: 'error',
      data: err
    })
  }
}
