const bcrypt = require('bcrypt-nodejs')
const Universe = require('../models/universeModel')
const User = require('../models/userModel')
const constructFactory = require('../factory/constructFactory')
const alienFactory = require('../factory/alienFactory')

async function hashPassword (pass) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(pass, '', null, (err, hash) => {
      if (err) {
        reject(err)
      }
      resolve(hash)
    })
  })
}

exports.index = function (req, res) {
  if (!req.session.user) {
    return res
      .status(500)
      .send({
        status: 'error',
        user: 'FORBIDDEN'
      })
      .end()
  }
  User.get(function (err, users) {
    if (err) {
      res
        .status(500)
        .send({
          status: 'error',
          user: err
        })
        .end()
    }

    res.json({
      status: 'success',
      users: users
    })
  })
}

exports.create = async function (req, res) {
  if (!req.body.username || !req.body.password) {
    res
      .status(500)
      .send({
        status: 'error',
        user: 'Make sure that you send all the required data'
      })
      .end()
  }
  try {
    let existingUserWithGivenUSername = await User.findOne({ username: req.body.username })
    if (existingUserWithGivenUSername) {
      res.status(423).send({
        status: 'error',
        user: 'Username is already used.'
      })
    } else {
      let user = new User()
      user.username = req.body.username
      user.password = await hashPassword(req.body.password)
      user.new = true
      var universe = new Universe()
      universe.constructs = await constructFactory.create()
      universe.aliens = await alienFactory.create()
      await universe.save()
      user.universe = universe

      user.save((err) => {
        if (err) {
          return res
            .status(500)
            .send({
              status: 'error',
              user: err
            })
            .end()
        } else {
          user.password = null
          return res
            .json({
              status: 'success',
              user: user
            })
            .end()
        }
      })
    }
  } catch (err) {
    console.log(err)
    return res
      .status(500)
      .send({
        status: 'error',
        user: err
      })
      .end()
  }
}

exports.update = async function (req, res) {
  if (!req.session.user || req.params.user_id !== req.session.user._id) {
    return res
      .status(500)
      .send({
        status: 'error',
        user: 'You are not allowed to update this user'
      })
      .end()
  }

  try {
    let user = await User.findOne({ _id: req.params.user_id })
    if (req.body.username && req.body._id !== user._id) {
      let checkUsername = await User.findOne({ username: req.body.username })
      if (checkUsername && checkUsername._id !== user._id) {
        return res.status(423).send({
          status: 'error',
          user: 'Username already used'
        })
      }
    } else {
      user.username = req.body.username || user.username
      if (req.body.password) {
        user.password = await hashPassword(req.body.password)
      }

      if (req.body.new !== null) {
        user.new = req.body.new
      }

      user.save().catch((err) => {
        console.log(err)
        return res.status(500).send({
          status: 'error',
          user: err
        })
      })

      return res.json({
        status: 'success',
        user: user
      })
    }
  } catch (err) {
    console.log(err)
    return res.status(500).send({
      status: 'error',
      user: err.message
    })
  }
}
