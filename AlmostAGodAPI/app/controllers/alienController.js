const Alien = require('../models/alienModel')
exports.create = function (req, res) {
  if (!req.body.number || !req.body.name || !req.body.base_stardust || !req.body.atomGain) {
    res
      .status(404)
      .send({
        status: 'error',
        data: 'Make sure that you send all the required data'
      })
      .end()
  } else {
    var alien = new Alien()
    alien.number = req.body.number
    alien.name = req.body.name
    alien.base_stardust = req.body.base_stardust
    alien.atomGain = req.body.atomGain
    alien.save(function (err) {
      if (err) {
        res
          .status(500)
          .send({
            status: 'error',
            data: 'Something wrong happened'
          })
          .end()
      } else {
        res.json({ status: 'success', data: alien })
      }
    })
  }
}
exports.index = function (req, res) {
  Alien.get(function (err, alien) {
    if (err) {
      res.status(500).send({ status: 'error', data: err }).end()
    }
    res.json({
      status: 'success',
      data: alien
    })
  })
}
exports.get = function (req, res) {
  Alien.findOne({ _id: req.params.alien_id }).exec(function (err, alien) {
    if (err) {
      res.status(500).send({ status: 'error', data: err }).end()
    }
    res.json({
      status: 'success',
      data: alien
    })
  })
}
