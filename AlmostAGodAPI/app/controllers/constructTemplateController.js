const TemplateConstruct = require('../models/constructTemplateModel')

exports.create = function (req, res) {
  if (!req.body.basePrice || !req.body.name || !req.body.baseMicrowave || !req.body.atomGain || !req.body.number) {
    res
      .status(404)
      .send({
        status: 'error',
        data: 'Make sure that you send all the required data'
      })
      .end()
  } else {
    var construct = new TemplateConstruct()
    construct.number = req.body.number
    construct.name = req.body.name
    construct.basePrice = req.body.basePrice
    construct.baseMicrowave = req.body.baseMicrowave
    construct.atomGain = req.body.atomGain
    construct.save(function (err) {
      if (err) {
        res
          .status(500)
          .send({
            status: 'error',
            data: err
          })
          .end()
      } else {
        res.json({ status: 'success', data: construct })
      }
    })
  }
}

exports.index = function (req, res) {
  TemplateConstruct.get(function (err, construct) {
    if (err) {
      res.status(500).send({ status: 'error', data: err }).end()
    } else {
      res.json({
        status: 'success',
        data: construct
      })
    }
  })
}
exports.get = function (req, res) {
  TemplateConstruct.findOne({ _id: req.params.constructtemplate_id }).exec(function (err, construct) {
    if (err) {
      res.status(500).send({ status: 'error', data: err }).end()
    } else {
      res.json({
        status: 'success',
        data: construct
      })
    }
  })
}
