const Construct = require('../models/constructModel')

exports.index = function (req, res) {
  Construct.get(function (err, construct) {
    if (err) {
      res.status(500).send({ status: 'error', data: err }).end()
    } else {
      res.json({
        status: 'success',
        data: construct
      })
    }
  })
}
exports.get = function (req, res) {
  Construct.findOne({ _id: req.params.construct_id }).exec(function (err, construct) {
    if (err) {
      res.status(500).send({ status: 'error', data: err }).end()
    } else {
      res.json({
        status: 'success',
        data: construct
      })
    }
  })
}
