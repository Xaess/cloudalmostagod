const bcrypt = require('bcrypt-nodejs')
const User = require('../models/userModel')
const Universe = require('../models/universeModel')

async function comparePasswordToHash (providedPassword, hashedPassword) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(providedPassword, hashedPassword, (err, result) => {
      if (err) { reject(err) }
      resolve(result)
    })
  })
}

exports.login = async function (req, res) {
  var providedPassword = req.body.password
  var providedUsername = req.body.username

  if (!providedPassword || !providedUsername) {
    return res.status(500).send({
      status: 'error',
      data: 'no data'
    })
  } else {
    try {
      let user = await User.findOne({ username: providedUsername }).populate('team').populate('universe')
      if (user) {
        let universe = await Universe.findOne({ _id: user.universe }).populate('constructs').populate('aliens')

        user.universe = universe
        let checkPassword = await comparePasswordToHash(providedPassword, user.password)
        if (checkPassword) {
          user.password = null
          req.session.user = user

          return res.json({
            status: 'success',
            data: user
          })
        }
      }
    } catch (err) {
      console.log(err)
    }

    // default response
    return res.status(500).send({
      status: 'error',
      data: 'Invalid credentials.'
    })
  }
}
