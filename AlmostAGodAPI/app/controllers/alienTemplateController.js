const TemplateAlien = require('../models/alienTemplateModel')

exports.create = async (req, res) => {
  if (!req.body.number || !req.body.name || !req.body.base_stardust || !req.body.atomGain) {
    return res.status(404).send({
      status: 'error',
      data: 'Make sure that you send all the required data'
    })
  }

  try {
    let alien = new TemplateAlien()
    alien.number = req.body.number
    alien.name = req.body.name
    alien.base_stardust = req.body.base_stardust
    alien.atomGain = req.body.atomGain
    alien = await alien.save()
    return res.json({
      status: 'success',
      data: alien
    })
  } catch (err) {
    return res.status(500).send({
      status: 'error',
      data: 'Something wrong happened'
    })
  }
}

exports.index = async (req, res) => {
  try {
    let templatesAlien = await TemplateAlien.find()
    return res.json({
      status: 'success',
      data: templatesAlien
    })
  } catch (err) {
    return res.status(500).send({
      stauts: 'error',
      data: err
    })
  }
}

exports.get = async (req, res) => {
  try {
    let templateAlien = await TemplateAlien.findById(req.params.template_alien_id)
    return res.json({
      status: 'success',
      data: templateAlien
    })
  } catch (err) {
    return res.status(500).send({
      status: 'error',
      data: err
    })
  }
}
