const Universe = require('../models/universeModel')
const Construct = require('../models/constructModel')
const Alien = require('../models/alienModel')
const User = require('../models/userModel')
const constructFactory = require('../factory/constructFactory')
const alienFactory = require('../factory/alienFactory')
exports.index = function (req, res) {
  Universe.get(function (err, universe) {
    if (err) {
      res.status(404).send({ status: 'error', data: err }).end()
    }
    res.json({
      status: 'success',
      data: universe
    })
  })
}
exports.update = async function (req, res) {
  try {
    let universe = await Universe.findOne({ _id: req.params.universe_id }).populate('constructs').populate('aliens')
    universe.atom = req.body.atom || universe.atom
    universe.microwave = req.body.microwave || universe.microwave
    universe.atomGain = req.body.atomGain || universe.atomGain
    universe.stardust = req.body.stardust || universe.stardust
    if (req.body.constructs) {
      for (let construct of req.body.constructs) {
        try {
          let cons = await Construct.findById(construct._id)
          cons.number = construct.number || cons.number
          cons.atomGain = construct.atomGain || cons.atomGain
          await cons.save()
        } catch (err) {
          res.status(500).send({
            status: 'error',
            data: err
          })
        }
      }
    }

    if (req.body.aliens) {
      for (let alien of req.body.aliens) {
        try {
          let ali = await Alien.findById(alien._id)
          ali.number = alien.number || ali.number
          ali.atomGain = alien.atomGain || ali.atomGain
          await ali.save()
        } catch (err) {
          res.status(500).send({
            status: 'error',
            data: err
          })
        }
      }
    }
    try {
      await universe.save()
      res.json({
        status: 'success',
        data: universe
      })
    } catch (err) {
      res
        .status(500)
        .send({
          status: 'error',
          data: err
        })
        .end()
    }
  } catch (err) {
    return res
      .status(404)
      .send({
        status: 'error not found',
        data: err
      })
      .end()
  }
}
exports.prestige = async function (req, res) {
  try {
    let user = await User.findOne({ _id: req.session.user._id })
    // Reset universe
    var universe = new Universe()
    universe.constructs = await constructFactory.create()
    universe.aliens = await alienFactory.create()
    await universe.save()
    user.universe = universe
    await user.save()
    res.json({
      status: 'success',
      data: universe
    })
  } catch (err) {
    return res
      .status(404)
      .send({
        status: 'Not User found',
        data: err
      })
      .end()
  }
}
exports.get = async function (req, res) {
  try {
    let universe = await Universe.findOne({ _id: req.params.universe_id }).populate('constructs').populate('aliens')
    return res.json({
      status: 'success',
      data: universe
    })
  } catch (err) {
    return res.status(500).send({
      status: 'error',
      data: 'err'
    })
  }
}
