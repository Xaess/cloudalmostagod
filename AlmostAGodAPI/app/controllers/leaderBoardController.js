const Team = require('../models/teamModel')

exports.get = async function (req, res) {
  try {
    let teams = await Team.find()
    if (!teams || teams.lenght === 0) {
      return res.status(404).send({
        status: 'error',
        data: 'No teams found'
      })
    }
    let leaderBoard = await teams.sort((a, b) => b.total_microwave - a.total_microwave)
    return res.json({
      status: 'success',
      data: leaderBoard
    })
  } catch (err) {
    console.log(err.message)
    return res.status(500).send({
      status: 'error',
      data: err
    })
  }
}
