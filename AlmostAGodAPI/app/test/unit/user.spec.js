const User = require('../../models/userModel')
const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../app')
const assert = require('chai').assert

chai.use(chaiHttp)

describe('Users', () => {
  beforeEach((done) => {
    User.remove({}, (err) => {
      if (err !== null) {
        throw err
      }
      done()
    })
  })

  describe('log user', () => {
    it('it shouldnt log the user', (done) => {
      chai.request(server)
        .post('/players/login')
        .end((err, res) => {
          if (err !== null) {
            throw err
          }
        })
        .then(function (res) {
          assert.equal(res.status, 500)
          done()
        })
        .catch(function (err) {
          return done(err)
        })
    })
  })

  describe('/GET user', () => {
    it('it should GET all the users', (done) => {
      chai.request(server)
        .get('/players/users')
        .end((err, res) => {
          if (err !== null) {
            throw err
          }
          assert.equal(res.status, 200)
          assert.typeOf(res.body, 'array')
          assert.lengthOf(res.body, 0)
          done()
        })
    })
  })
})
