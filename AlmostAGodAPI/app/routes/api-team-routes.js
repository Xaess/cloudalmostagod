let router = require('express').Router()
const teamController = require('../controllers/teamController')

router.route('/')
  .get(teamController.index)
  .post(teamController.create)

router.route('/join/:team_id')
  .patch(teamController.join)

router.route('/leave/:team_id')
  .patch(teamController.leave)

router.route('/:team_id')
  .get(teamController.get)
  .patch(teamController.update)

module.exports = router
