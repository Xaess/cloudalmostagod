// Initialize express router
let router = require('express').Router()

// Import controllers
var universeController = require('../controllers/universeController')

// Universe routes
router.route('/').get(universeController.index)
router.route('/prestige').post(universeController.prestige)

// Update game with atom and microwaves
router.route('/:universe_id').patch(universeController.update).get(universeController.get)

// Export API routes
module.exports = router
