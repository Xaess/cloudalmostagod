// Initialize express router
let router = require('express').Router()

// Import controllers
var authController = require('../controllers/authController')
var userController = require('../controllers/userController')
// login route
router.route('/login').post(authController.login)
// player creation and update routes
router.route('/register').post(userController.create)
router.route('/').get(userController.index)
router.route('/:user_id').patch(userController.update)
// Export Auth routes
module.exports = router
