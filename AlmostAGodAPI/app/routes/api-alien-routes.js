let router = require('express').Router()
var alienController = require('../controllers/alienController')
router.route('/').post(alienController.create).get(alienController.index)
router.route('/:alien_id').get(alienController.get)

module.exports = router
