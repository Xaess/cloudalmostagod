let router = require('express').Router()
var constructController = require('../controllers/constructController')
router.route('/').get(constructController.index)
router.route('/:construct_id').get(constructController.get)

module.exports = router
