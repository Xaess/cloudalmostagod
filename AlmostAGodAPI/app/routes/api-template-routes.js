let router = require('express').Router()
var templateConstructController = require('../controllers/constructTemplateController')
var templateAlienController = require('../controllers/alienTemplateController')
router.route('/construct').post(templateConstructController.create).get(templateConstructController.index)
router.route('/construct/:constructtemplate_id').get(templateConstructController.get)
router.route('/alien').post(templateAlienController.create).get(templateAlienController.index)
router.route('/alien/:template_alien_id').get(templateAlienController.get)

module.exports = router
