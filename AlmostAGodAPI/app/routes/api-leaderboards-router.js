let router = require('express').Router()
var leaderbordController = require('../controllers/leaderBoardController')
router.route('/').get(leaderbordController.get)

module.exports = router
